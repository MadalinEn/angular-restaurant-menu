export const EXTRAS: any[] = [
	{
		type: 'toppings',
		items: [
			{
				name: 'corn',
				prce: 0.7
			},
			{	
				name: 'mozzarella',
				price: 1.2
			}
		]
	},
	{
		type: 'crust',
		items: [
			'Fluffy',
			'Crispy'
		]
	}
	
];