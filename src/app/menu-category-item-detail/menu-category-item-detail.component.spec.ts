import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCategoryItemDetailComponent } from './menu-category-item-detail.component';

describe('MenuCategoryItemDetailComponent', () => {
  let component: MenuCategoryItemDetailComponent;
  let fixture: ComponentFixture<MenuCategoryItemDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuCategoryItemDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCategoryItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
