import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MENU } from "../menu-repo";
import { EXTRAS } from "../menu-extras-repo";
import { MenuCategory } from '../menu-category';
import { MenuCategoryItem } from '../menu-category-item';

@Component({
	selector: 'menu-item-detail',
	templateUrl: './menu-category-item-detail.component.html',
	styleUrls: ['./menu-category-item-detail.component.scss']
})
export class MenuCategoryItemDetailComponent implements OnInit {

	@Input() category: MenuCategory;
	@Input() item: MenuCategoryItem;

	extras = EXTRAS;

	constructor(
		private route: ActivatedRoute,
		private location: Location
	) {}

	ngOnInit(): void {
		this.setMenuCategory();
		this.setMenuCategoryItem();
	}

	setMenuCategory(): void {
		const categoryId = +this.route.snapshot.paramMap.get('categoryId');
		this.category = new MenuCategory(MENU.find(category => category.id === categoryId));
		// console.log(this.category);
	}

	setMenuCategoryItem(): void {
		const itemId = +this.route.snapshot.paramMap.get('itemId');
		this.item = this.category.items.find(item => item.id === itemId);
		// console.log(this.item);
	}

	toggleExtraTopping(args): void {
		var toppings = this.extras.find(extras => extras.type == 'toppings').items;
		console.log(toppings);
		// this.item.price += topping.price;
	}

	goBack(): void {
		this.location.back();
	}

}
