import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { MenuCategoriesComponent } from './menu-categories/menu-categories.component';
import { MenuCategoryItemsComponent } from './menu-category-items/menu-category-items.component';
import { MenuCategoryItemDetailComponent } from './menu-category-item-detail/menu-category-item-detail.component';
import { AppRoutingModule } from './/app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuCategoriesComponent,
    MenuCategoryItemsComponent,
    MenuCategoryItemDetailComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
