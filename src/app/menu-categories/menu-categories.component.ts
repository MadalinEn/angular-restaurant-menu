import { Component, OnInit } from '@angular/core';

import { MENU } from "../menu-repo";
import { MenuCategory } from '../menu-category';

@Component({
	selector: 'menu-categories',
	templateUrl: './menu-categories.component.html',
	styleUrls: ['./menu-categories.component.scss']
})
export class MenuCategoriesComponent implements OnInit {

	menu = MENU;

	constructor() { }

	ngOnInit() {
	}

}
