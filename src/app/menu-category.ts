import { MenuCategoryItem } from './menu-category-item';

export class MenuCategory {
	id: number;
	name: string;
	image: string;
	extra: object;
	items: any[] = [];

	constructor(args: any) { 
		this.id    = args.id; 
		this.name  = args.name;
		this.image = args.image;
		this.extra  = args.extra;
		
		for(let item of args.items) {
			this.items.push(new MenuCategoryItem(item));
		}
	}
}
