import { MenuCategory } from './menu-category';

export const MENU: MenuCategory[] = [
	{
		id: 0,
		name: "Pizza",
		image : "pizza.jpg",
		extra : [],
		items: [
			{
				id: 0,
				name: "Pizza Margherita",
				image : "pizza.jpg",
				info : "tomato sauce, cheese",
				price : 7,
				extra : []
			},
			{
				id: 1,
				name: "Pizza Prosciutto",
				image : "pros_image",
				info : "tomato sauce, cheese, ham",
				price : 10,
				extra : [{"crust" : "fluffy"}]
			}
		]
	},
	{
		id: 1,
		name: "Pasta",
		image : "pasta_image",
		extra : [],
		items: [
			{
				id: 0,
				name: "Spaghetti Bolognese",
				image : "pizza.jpg",
				info : "",
				price : 9,
				extra : []
			},
			{
				id: 1,
				name: "Spaghetti Frutti di Mare",
				image : "pros_image",
				info : "with seafood",
				price : 12,
				extra : []
			}
		]
	},	
];