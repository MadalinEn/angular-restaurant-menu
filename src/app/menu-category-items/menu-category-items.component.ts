import { Component, OnInit, Input } from '@angular/core';

import { MenuCategory } from '../menu-category';
import { MenuCategoryItem } from '../menu-category-item';

@Component({
	selector: 'menu-category',
	templateUrl: './menu-category-items.component.html',
	styleUrls: ['./menu-category-items.component.scss']
})
export class MenuCategoryItemsComponent implements OnInit {

	@Input('item') item: MenuCategoryItem;
	@Input('categoryId') categoryId: number;

	constructor() { }

	ngOnInit() {
	}

}
