export class MenuCategoryItem {
	id: number;
	name: string;
	image: string;
	info: string;
	price: number;
	extra: object;

	constructor(args: any) { 
		this.id    = args.id; 
		this.name  = args.name;
		this.image = args.image;
		this.info  = args.info;
		this.price = args.price;
		this.extra  = args.extra;
	}
}
